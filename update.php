
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>修改数据</title>
    <link rel="stylesheet" href="./css/bootstrap.css">
</head>

<body>
<?php
//1. 引入链接 conn.php
require_once './conn.php';
// 2.判断有没有post提交
if (count($_POST)>0) {
    // 获取post内容 
    // 执行sql语句跟新数据
    mysqli_query($link,"UPDATE users set name='" . $_POST['username'] . "', mobile='" . $_POST['mobile'] . "' ,email='" . $_POST['email'] . "' WHERE id='" . $_POST['id'] . "'");
     // 跳转退出
    header("location: index.php"); 
    exit();
}
// 获取链接传递的数据 id
$id=$_GET['id'];
//获取id 从数据库 所有字段信息
$sql="select * from users where id = '".$id."'";
$result = mysqli_query($link,$sql);
//构造sql select * from users where id=?
$row=mysqli_fetch_array($result);
// var_dump($row);
//把数据放在表单 value


// UI界面

?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                <h1>修改数据</h1>
                <form method="post" action="./update.php">
                    <div class="form-group">
                        <label for="username">姓名</label>
                        <input type="text" class="form-control" id="username" name="username" value="<?php echo $row['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">邮箱</label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $row['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="mobile">电话号码</label>
                        <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $row['mobile']; ?>">
                    </div>
                    <input type="hidden" name="id" value="<?php echo $row["id"]; ?>"/>
                    <button type="submit" class="btn btn-primary" name="update">修改</button>
                    <a href="./index.php" class="btn btn-success">返回</a>
                </form>
            </div>
        </div>
    </div>


</body>

</html>