<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/font-awesome.min.css">
</head>
<body>
  <div>  <h1>用户列表</h1>
    <a href="./insert.html" class="btn btn-success"><i class="fa fa-plus-square fa-fw"></i>新增用户</a></div>
  
    <!-- 数据展示 -->
    <?php
    //引入数据库文件
    require_once './conn.php';
    $result = mysqli_query($link,"select * from users");
    if(mysqli_num_rows($result)>0){
    ?>
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>序号</th>
        <th>ID</th>
        <th>姓名</th>
        <th>邮箱</th>
        <th>电话号码</th>
        <th>操作</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $i=1;
    // $row=mysqli_fetch_array($result);
    // var_dump($row);
    while ($row=mysqli_fetch_array($result)) {
    
    ?>
      <tr>
        <td><?php echo $i ;?></td>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><?php echo $row['mobile'];?></td>
        <td>  
            <a href="update.php?id=<?php echo $row['id']; ?>"  class="btn btn-warning"> 修改</a>
            <a href="" class="btn btn-danger">删除</a>
        </td>
      </tr>
    <?php
    $i++;
    }
    ?>
    </tbody>
</table>
<?php
      }
      else{
          echo "<p>无数据</p>";
      }
      ?>
    </body>
</html>